# Java Prod

Container for exec Java projects.   

Ubuntu 20.04 based:
- Java 8  
- Java 11  

Ubuntu 22.04 based:
- Java 8  

Debian 11.5 (bullseye) based:
- Java 11

Debian 12.5 (bookworm) based:
- Java 21


**Packages:**

- Java JRE  
- ca-certificates  
